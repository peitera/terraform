variable "aws_region" {
  default = "eu-west-2"
}

variable "aws_ubuntu" {
  default = "ami-0287acb18b6d8efff"
}

variable "worker_instance" {
  default = "t2.micro"
}

variable "master_instance" {
  default = "t2.micro"
}

variable "aws_keyfile" {
  default = "files/id_rsa.pub"
}

variable "worker_count" {
  default = 3
}

variable "master_count" {
  default = 1
}

variable "vpc_dev_cidr" {
  default = "10.244.5.0/24"
}

variable "sub_pub_cidr" {
  default = "10.244.0.0/24"
}