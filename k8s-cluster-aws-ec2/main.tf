provider "aws" {
  profile = "default"
  region  = var.aws_region
}

resource "aws_key_pair" "k8s" {
  key_name   = "k8s"
  public_key = file(var.aws_keyfile)
}

resource "aws_vpc" "k8s_dev_vpc" {
  cidr_block           = var.vpc_dev_cidr
  enable_dns_hostnames = true

  tags = {
    Name = "k8s-dev"
  }
}

resource "aws_security_group" "k8s" {
  name        = "k8s-security-group"
  description = "Allow k8s traffic"
  vpc_id      = aws_vpc.k8s_dev_vpc.id

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "k8s cluster"
    from_port   = 6443
    to_port     = 6443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "k8s cluster"
    from_port   = 8181
    to_port     = 8181
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "k8s cluster"
    from_port   = 10250
    to_port     = 10250
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "k8s cluster"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "k8s-dev"
  }
}

resource "aws_subnet" "k8s_dev_int" {
  vpc_id                  = aws_vpc.k8s_dev_vpc.id
  cidr_block              = var.vpc_dev_cidr
  map_public_ip_on_launch = true

  tags = {
    Name = "k8s-dev-int"
  }
}

resource "aws_internet_gateway" "int_gateway" {
  vpc_id = aws_vpc.k8s_dev_vpc.id

  tags = {
    Name = "int-gateway-k8s"
  }
}

resource "aws_route_table" "k8s_route" {
  vpc_id = aws_vpc.k8s_dev_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.int_gateway.id
  }

  tags = {
    Name = "k8s_dev_gw"
  }
}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.k8s_dev_int.id
  route_table_id = aws_route_table.k8s_route.id
}

resource "aws_instance" "k8s_worker" {
  key_name      = aws_key_pair.k8s.key_name
  ami           = var.aws_ubuntu
  instance_type = var.worker_instance
  count         = var.worker_count
  subnet_id     = aws_subnet.k8s_dev_int.id

  tags = {
    Name = "k8s-worker-dev-${count.index + 1}"
    node = "worker"
  }

  vpc_security_group_ids = [
    aws_security_group.k8s.id
  ]

  ebs_block_device {
    device_name = "/dev/sda1"
    volume_type = "gp2"
    volume_size = 30
  }

  user_data = data.template_cloudinit_config.worker.*.rendered[count.index]
}


data "template_cloudinit_config" "worker" {
  gzip          = true
  base64_encode = true
  count         = var.worker_count

  part {
    content_type = "text/cloud-config"
    content      = <<EOF
    preserve_hostname: false
    fqdn: "k8s-worker-dev-${count.index + 1}"
    hostname: "k8s-worker-dev-${count.index + 1}"
EOF
  }
}

resource "aws_instance" "k8s_master" {
  key_name      = aws_key_pair.k8s.key_name
  ami           = var.aws_ubuntu
  instance_type = var.master_instance
  count         = var.master_count
  subnet_id     = aws_subnet.k8s_dev_int.id

  tags = {
    Name = "k8s-master-dev-${count.index + 1}"
    node = "master"
  }

  vpc_security_group_ids = [
    aws_security_group.k8s.id
  ]

  ebs_block_device {
    device_name = "/dev/sda1"
    volume_type = "gp2"
    volume_size = 30
  }

  user_data = data.template_cloudinit_config.master.*.rendered[count.index]
}

data "template_cloudinit_config" "master" {
  gzip          = true
  base64_encode = true
  count         = var.master_count

  part {
    content_type = "text/cloud-config"
    content      = <<EOF
    preserve_hostname: false
    fqdn: "k8s-master-dev-${count.index + 1}"
    hostname: "k8s-master-dev-${count.index + 1}"
EOF
  }
}


