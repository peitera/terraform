
terraform {
  required_providers {
    proxmox = {
      source  = "Telmate/proxmox"
      version = "2.6.7"
    }
  }
}

provider "proxmox" {
  pm_tls_insecure = true
  pm_api_url      = "https://${var.prox_api_url}/api2/json"
  pm_user         = var.prox_user
  pm_password     = var.prox_password
}

resource "proxmox_vm_qemu" "vm" {
  count       = 1
  name        = "${var.vm_name}-${count.index}"
  target_node = "pve"
  clone       = var.vm_clone
  full_clone  = true
  cores       = var.vm_cores
  sockets     = "1"
  cpu         = "host"
  memory      = var.vm_mem
  scsihw      = "virtio-scsi-pci"
  bootdisk    = "scsi0"
  disk {
    size    = "${var.vm_disk_size}G"
    type    = "scsi"
    storage = var.vm_disk_drive
  }
  network {
    model  = "virtio"
    bridge = "vmbr0"
  }
  lifecycle {
    ignore_changes = [
      network,
    ]
  }
}